# Tools for Learning Foreign Language

prerequisites
- sox
- audacity


## 1. How to split long duration audio file into sentences (FASTLY)

Open audio file on audacity.

Do like below.

>play - pause - add label - deactivate cursor - pause - add label - deactivate cursor - ...(repeat)...

On windows, play=<space>, pause=<p>, add label=<m>, deacti. cursor=<esc>

On mac, play=<space>, pause=<p>, add label=<cmd+.>, deacti. cursor=<esc>


After adding all labels, Select File > Export Multiple.

>*Caution*: The number of segments should be less than 100. "Autonumbering to filename" supports only "2-digit zero-padded" number.

## 2. How to pad silence back to mp3 file

Prepare mp3 file, mp3 filepath list. The mp3 file has a mono channel.

The filepath list looks like below.

```
sentence_1.mp3
sentence_2.mp3
...
```

Then execute.
```
bash pad_post_sil.sh <mp3filelist>
```
There might be today.mp3.list, padded mp3 files.

This program is under BEER-WARE LICENSE.

Good Luck!
