
for orig in `cat $1` 
do 
	samplerate=`sox --i $orig | grep "Sample Rate" | gawk '{print $4}'`
	duration=`sox --i $orig | grep Duration | gawk '{print $3}' | gawk -F: '{print $3}'` 
  echo $duration $samplerate
	sox -n -r $samplerate -c 1 tmp_sil.mp3 trim 0.0 $duration
	sox $orig tmp_sil.mp3 pad_sil_$orig 
	echo "$orig + silence => pad_sil_$orig"
	echo "pad_sil_$orig" >> today.mp3.list

done
